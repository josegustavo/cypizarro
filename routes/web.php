<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });

// Para activar uso de sesiones
// $app = $router->app;
// $app->middleware([
//    \Illuminate\Session\Middleware\StartSession::class
// ]);
// $app->bind(\Illuminate\Session\SessionManager::class, function () use ($app) {
//     return new \Illuminate\Session\SessionManager($app);
// });
// $app->configure('session');
// $app->register(\Illuminate\Session\SessionServiceProvider::class);

$router->get('/', function(Illuminate\Http\Request $request){
    return app('auth')->guest()?"invitado":"autenticado";
});

// Para validar autenticación
$router->group(['prefix' => 'admin','middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'AdminController@home');
    $router->get('/productos', 'AdminController@productos');
    $router->get('/servicios', 'AdminController@servicios');
    $router->get('/sostenibilidad', 'AdminController@sostenibilidad');
    $router->get('/contacto', 'AdminController@contacto');

    $router->post('/save', 'AdminController@save');
});

$router->get('/login', 'LoginController@show');
$router->post('/login', 'LoginController@login');
$router->get('/logout', 'LoginController@logout');

