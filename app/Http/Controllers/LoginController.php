<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Cookie;

class LoginController extends Controller
{

    public function show(Request $request, Response $response)
    {
        $is_guest = app('auth')->guest();
        if( $is_guest ) {
            $auth_key = env('APP_KEY_ADMIN');
            $new_app_token = app('hash')->make($auth_key);
            $response->cookie(new Cookie('app-token', $new_app_token));
            return $response;
        }
        else {
            return redirect('/');
        }
    }

    public function login(Request $request, Response $response)
    {
        $auth_key = $request->post('app-token');
        if( $auth_key === env('APP_KEY_ADMIN')){
            $response->cookie(new Cookie('app-token', app('hash')->make($auth_key)));
            return $response;
        }
        return response('Unauthorized.', 401);
    }

    public function logout()
    {
        return redirect('/')->withCookie(new Cookie('app-token'));
    }

    
}